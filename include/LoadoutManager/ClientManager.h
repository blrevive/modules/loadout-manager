#pragma once
#include <LoadoutManager/LoadoutManager.h>
#include <LoadoutManager/LoadoutCache.h>

namespace BLRE::Loadout
{
	class ClientManager : public LoadoutManager
	{
	public:
		ClientManager(BLRE::BLRevive* blre);

		virtual void Initialize() override;

		void DETOUR_CB_CLS(SetupLoadout, AFoxPC, SetupLoadout);
		bool DETOUR_CB_CLS(IsItemPurchased, AFoxServerConnection, IsItemPurchased);
		void DETOUR_CB_CLS(ChangeSelectedLoadoutByID, AFoxPC, ChangeSelectedLoadoutByID);

	protected:
		std::string _PlayerName = "default";
		std::unique_ptr<LoadoutCache> CachedLoadouts;
	};
}