#pragma once
#include <BLRevive/Component.h>
#include <SdkHeaders.h>
#include <LoadoutManager/PlayerLoadout.h>
#include <LoadoutManager/UnlockablesCache.h>
#include <BLRevive/Utils.h>
#include <filesystem>

namespace BLRE::Loadout
{
	/**
	 * @brief Cache for loadouts
	*/
	class LoadoutCache : BLRE::Component
	{
	public:
		LoadoutCache();

		/**
		 * @brief get cached loadout by id (also checks for updates of the profile file)
		 * @param loadoutId loadout id
		 * @return cached loadout
		*/
		FLoadoutInfoData GetCachedLoadoutInfo(std::string loadoutId, AFoxPC* pc);

		/**
		 * @brief add or update loadout from config
		 * @param index loadout index
		 * @param config config from json
		*/
		void UpdateLoadout(int index, LoadoutConfig config, AFoxPC* pc);

		/**
		 * @brief load and cache profile file
		 * @param profileName name of profile to load
		*/
		void LoadProfileFile(std::string profileName, AFoxPC* pc);

	protected:
		// cached loadout metadata
		std::unordered_map<int, FLoadoutMetaData> CachedLoadoutMeta = {};
		// cached loadout info
		std::unordered_map<std::string, FLoadoutInfoData> CachedLoadoutInfo = {};
		// unlockable item cache
		std::shared_ptr<UnlockablesCache> UnlockCache;
		// path to directory containing profile files
		inline static const std::string ProfileDirectory = BLRE::Utils::FS::BlreviveConfigPath() + "\\profiles\\";
		// path to currently loaded profile file
		std::filesystem::path ProfileFilePath;
		// last write time of current profile file
		std::filesystem::file_time_type ProfileLastModifiedTime;

		/**
		 * @brief create loadout metadata from config
		 * @param index loadout index
		 * @param config loadout config
		 * @return loadout metadata
		*/
		FLoadoutMetaData CreateLoadoutMetaDataFromConfig(int index, LoadoutConfig config);
	};
}