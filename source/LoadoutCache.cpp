#include <LoadoutManager/LoadoutCache.h>
#include <nlohmann/json.hpp>
#include <iostream>
#include <fstream>
#include <BLRevive/BLRevive.h>

using namespace BLRE::Loadout;
using json = nlohmann::json;

LoadoutCache::LoadoutCache()
	: Component("LoadoutCache")
{
	UnlockCache = std::make_shared<UnlockablesCache>();
	UnlockCache->BuildCache();
}

FLoadoutInfoData LoadoutCache::GetCachedLoadoutInfo(std::string loadoutId, AFoxPC* pc)
{
	auto fileTime = std::filesystem::last_write_time(ProfileFilePath);
	if (fileTime > ProfileLastModifiedTime)
		LoadProfileFile(BLRevive::GetInstance()->URL.GetParam("Name", "default"), pc);

	auto it = CachedLoadoutInfo.find(loadoutId);
	if (it == CachedLoadoutInfo.end())
	{
		Log->error("failed to get cached loadout {}", loadoutId);
		return {};
	}

	return (*it).second;
}

void LoadoutCache::UpdateLoadout(int index, LoadoutConfig config, AFoxPC* pc)
{
	static auto storeData = UObject::GetInstanceOf<UFoxDataStore_StoreData>(true);

	Log->debug("updating loadout at {}", index);

	UFoxItemCacheLoadouts* loadoutsCache;
	if (!storeData->eventGetCachedLoadouts(&loadoutsCache))
	{
		Log->error("failed to get loadout cached while updating loadout {}", index);
		return;
	}

	auto metaData = CreateLoadoutMetaDataFromConfig(index, config);
	CachedLoadoutMeta[index] = metaData;

	const char* loadoutId = metaData.LoadoutId.ToChar();
	int cachedLoadoutIndex = -1;
	for (int i = 0; i < loadoutsCache->CachedLoadoutItems.Count; i++)
	{
		if (strcmp(loadoutsCache->CachedLoadoutItems[i].LoadoutId.ToChar(), loadoutId) == 0)
		{
			cachedLoadoutIndex = i;
			break;
		}
	}

	if (cachedLoadoutIndex == -1)
		loadoutsCache->CachedLoadoutItems.push_back(metaData);
	else
		loadoutsCache->CachedLoadoutItems[cachedLoadoutIndex] = metaData;

	FLoadoutInfoData loadoutInfo;
	if (!loadoutsCache->RetrieveLoadoutByID(metaData.LoadoutId, &loadoutInfo))
	{
		Log->error("failed to retrieve cached loadout while updating loadout {}", metaData.LoadoutId.ToChar());
		return;
	}

	UnlockCache->ConvertUIDsToIndices(loadoutInfo);

	CachedLoadoutInfo[metaData.LoadoutId.ToChar()] = loadoutInfo;
	Log->info("updated loadout {}", metaData.LoadoutId.ToChar());
}

FLoadoutMetaData LoadoutCache::CreateLoadoutMetaDataFromConfig(int index, LoadoutConfig loadout)
{
	FLoadoutMetaData loadoutMeta = {};
	loadoutMeta.CreateTime.Year = 2022;
	loadoutMeta.CreateTime.Month = 01;
	loadoutMeta.CreateTime.Day = 01;
	loadoutMeta.CreateTime.Hour = 00;
	loadoutMeta.CreateTime.Minute = 00;
	loadoutMeta.CreateTime.Second = 00;
	loadoutMeta.LastModifiedTime = loadoutMeta.CreateTime;

	loadoutMeta.CreatorID = FUniqueNetId{};
	loadoutMeta.CreatorName = "Player";
	loadoutMeta.InstalledCount = 1;
	loadoutMeta.Name = loadout.Name;
	loadoutMeta.OwnerID = {};
	loadoutMeta.ParentID = "";
	loadoutMeta.PrimaryWeaponID = loadout.Primary.Receiver;
	loadoutMeta.SecondaryWeaponID = loadout.Secondary.Receiver;
	loadoutMeta.LoadoutId = fmt::format("loadout-{0}", index);
	loadoutMeta.LoadoutDataStr = loadout.ToDataString();
	loadoutMeta.WeaponLoadoutNameInfo.PrimaryWeaponFriendlyName = loadout.PrimaryFriendlyName;
	loadoutMeta.WeaponLoadoutNameInfo.SecondaryWeaponFriendlyName = loadout.SecondaryFriendlyName;
	loadoutMeta.bIsLoadoutValid = true;

	return loadoutMeta;
}

void LoadoutCache::LoadProfileFile(std::string profileName, AFoxPC* pc)
{
	ProfileFilePath = ProfileDirectory + profileName + ".json";
	// open profile file
	std::ifstream file(ProfileFilePath);
	if (!file.good())
	{
		this->Log->debug("failed to load profile for {0}, using default profile", profileName);
		ProfileFilePath = ProfileDirectory + "default.json";
		file = std::ifstream(ProfileFilePath);
		if (!file.good())
		{
			this->Log->error("failed to load profile for {0} and default profile", profileName);
			return;
		}
	}

	// parse json to player profile
	try
	{
		// parse file to json
		json profile;
		file >> profile;
		file.close();

		// parse json profile to class
		int i = 0;
		for (auto& jloadout : profile)
			UpdateLoadout(i++, LoadoutConfig(jloadout), pc);

		ProfileLastModifiedTime = std::filesystem::last_write_time(ProfileFilePath);
	}
	catch (json::exception e)
	{
		Log->error("Failed to parse player loadout from json: {}", e.what());
		file.close();
	}
}