#include <LoadoutManager/LoadoutManager.h>
#include <LoadoutManager/resource.h>
#include <BLRevive/Resources.h>
#include <LoadoutManager/ClientManager.h>

#pragma warning(disable : 4244)
#include <SdkHeaders.h>
#include <LoadoutManager/SdkUtils.h>

#include <vector>
#include <sys/stat.h>
#include <sys/types.h>
#include <filesystem>

namespace fs = std::filesystem;
using json = nlohmann::json;

using namespace BLRE::Loadout;

void LoadoutManager::CheckDefaultFiles()
{
	struct stat Info;
	if (stat(ProfileDirectory.c_str(), &Info) != 0)
	{
		fs::create_directory(ProfileDirectory.c_str());
	}

	if (stat((ProfileDirectory + "default.json").c_str(), &Info))
	{
		// @todo create loadout file from LoadoutInfo::TheDefaultLoadouts instead of resource
		std::ofstream def(ProfileDirectory + "default.json");
		def << Resources::Get<std::string>(RES_LOADOUT_DEFAULT);
		def.close();
	}
}

void LoadoutManager::Initialize()
{
}


extern "C" __declspec(dllexport) void InitializeModule(BLRE::BLRevive * blre)
{
	if (BLRE::Utils::IsServer())
		return;

	auto clientManager = new ClientManager(blre);
	clientManager->Initialize();
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved)
{
	return true;
}