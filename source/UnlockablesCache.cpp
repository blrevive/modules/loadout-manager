#include <LoadoutManager/UnlockablesCache.h>

using namespace BLRE::Loadout;

UnlockablesCache::UnlockablesCache()
	: BLRE::Component("UnlockablesCache")
{

}

void UnlockablesCache::BuildCache()
{
	Log->debug("building unlockables cache");
	auto loadoutInfo = UObject::GetInstanceOf<AFoxLoadoutInfo>();
	auto uds = UObject::GetInstanceOf<UFoxDataStore_Unlockables>();
	auto unlockInfo = UObject::GetInstanceOf<UFoxUnlockInfo>(true);

	CacheDataProviderIndices(&uds->UpperBodyProviderArray);
	CacheDataProviderIndices(&uds->LowerBodyProviderArray);
	CacheDataProviderIndices(&uds->HelmetProviderArray);
	CacheDataProviderIndices(&uds->BadgeProviderArray);
	CacheDataProviderIndices(&uds->TacticalProviderArray);
	CacheDataProviderIndices(&uds->AttachmentProviderArray);
	CacheDataProviderIndices(&uds->PatchProviderArray);
	CacheDataProviderIndices(&uds->MagazineProviderArray);
	CacheDataProviderIndices(&uds->AmmoProviderArray);
	CacheDataProviderIndices(&uds->MuzzleProviderArray);
	CacheDataProviderIndices(&uds->AvatarProviderArray);
	CacheDataProviderIndices(&uds->AIAvatarProviderArray);
	CacheDataProviderIndices(&uds->CrosshairProviderArray);
	CacheDataProviderIndices(&uds->HangerProviderArray);
	CacheDataProviderIndices(&uds->PrimarySkinProviderArray);
	CacheDataProviderIndices(&uds->SecondarySkinProviderArray);
	CacheDataProviderIndices(&uds->HudSkinProviderArray);
	CacheDataProviderIndices(&uds->EmoteProviderArray);
	CacheUnlockInfoIndices(&unlockInfo->BodyCamoUnlocks);
	CacheUnlockInfoIndices(&unlockInfo->WeaponCamoUnlocks);
	CacheUnlockInfoIndices(&unlockInfo->TauntUnlocks);

	for (int i = 0; i < CONST_UNLOCK_DEPOT_ITEM_END - CONST_UNLOCK_DEPOT_ITEM_START; i++) {
		UIDToIndexMap[i + CONST_UNLOCK_DEPOT_ITEM_START] = i;
	}

	auto foxWeapons = UObject::GetInstancesOf<AFoxWeapon>([](AFoxWeapon* weapon) {
		return weapon->UnlockID > 0;
		});

	CacheWeaponProviderIndices(loadoutInfo->PrimaryWeaponNames, foxWeapons);
	CacheWeaponProviderIndices(loadoutInfo->SecondaryWeaponNames, foxWeapons);
	CacheBarrelProviderIndices();
	CacheStockProviderIndices();
	CacheScopeProviderIndices();
	CacheGripProviderIndices();
	CacheAmmoProviderIndices();

	std::vector<UFoxWeaponModObjectBase*> modObjects = UObject::GetInstancesOf<UFoxWeaponModObjectBase>([](UFoxWeaponModObjectBase* mod) {
		return mod->UnlockID > 0;
		});

	for (auto modObj : modObjects)
		WeaponMods[modObj->UnlockID] = modObj;
}

int UnlockablesCache::GetProviderIndexFromUID(int UID, int defaultIndex = -1)
{
	return UID > 0 ? UIDToIndexMap[UID] : defaultIndex;
}

void UnlockablesCache::ConvertUIDsToIndices(FLoadoutInfoData& loadoutData)
{
	Log->debug("converting item UIDs to indices for {}", loadoutData.LoadoutId);
	ConvertWeaponData(loadoutData);
	ConvertGearData(loadoutData);
	ConvertDepotData(loadoutData);
}

void UnlockablesCache::CacheWeaponProviderIndices(TArray<FString> weaponNames, std::vector<AFoxWeapon*> weapons)
{
	for (int i = 0; i < weaponNames.Count; i++)
	{
		const char* wpnName = weaponNames[i].ToChar();

		for (auto weapon : weapons)
		{
			const char* wpnClsName = weapon->Class->Name.GetName();
			if (strcmp(wpnClsName, wpnName) == 0)
			{
				UIDToIndexMap[weapon->UnlockID] = i;
				break;
			}
		}
	}
}

void UnlockablesCache::CacheBarrelProviderIndices()
{
	UIDToIndexMap[41000] = 1;
	UIDToIndexMap[41001] = 2;
	UIDToIndexMap[41002] = 3;
	UIDToIndexMap[41003] = 4;
	UIDToIndexMap[41004] = 5;
	UIDToIndexMap[41005] = 6;
	UIDToIndexMap[41006] = 7;
	UIDToIndexMap[41007] = 8;
	UIDToIndexMap[41008] = 9;
	UIDToIndexMap[41009] = 10;
	UIDToIndexMap[41010] = 11;
	UIDToIndexMap[41011] = 12;
	UIDToIndexMap[41012] = 13;
	UIDToIndexMap[41013] = 14;
	UIDToIndexMap[41014] = 15;
	UIDToIndexMap[41015] = 16;
	UIDToIndexMap[41016] = 17;
	UIDToIndexMap[41017] = 18;
	UIDToIndexMap[41018] = 19;
	UIDToIndexMap[41019] = 20;
	UIDToIndexMap[41020] = 0;
	UIDToIndexMap[41021] = 24;
	UIDToIndexMap[41022] = 25;
	UIDToIndexMap[41023] = 21;
	UIDToIndexMap[41024] = 22;
	UIDToIndexMap[41025] = 23;
	UIDToIndexMap[41026] = 26;
	UIDToIndexMap[41027] = 27;
	UIDToIndexMap[41028] = 28;
	UIDToIndexMap[41029] = 29;
	UIDToIndexMap[41030] = 30;
	UIDToIndexMap[41031] = 31;
	UIDToIndexMap[41032] = 32;
	UIDToIndexMap[41033] = 33;
	UIDToIndexMap[41034] = 34;
	UIDToIndexMap[41035] = 35;
	UIDToIndexMap[41036] = 36;
	UIDToIndexMap[41037] = 37;
	UIDToIndexMap[41038] = 38;
}

void UnlockablesCache::CacheScopeProviderIndices()
{
	UIDToIndexMap[45000] = 0;
	UIDToIndexMap[45001] = 1;
	UIDToIndexMap[45002] = 2;
	UIDToIndexMap[45003] = 3;
	UIDToIndexMap[45004] = 4;
	UIDToIndexMap[45005] = 5;
	UIDToIndexMap[45006] = 6;
	UIDToIndexMap[45007] = 7;
	UIDToIndexMap[45008] = 8;
	UIDToIndexMap[45009] = 9;
	UIDToIndexMap[45010] = 10;
	UIDToIndexMap[45011] = 12;
	UIDToIndexMap[45013] = 13;
	UIDToIndexMap[45014] = 14;
	UIDToIndexMap[45015] = 15;
	UIDToIndexMap[45016] = 16;
	UIDToIndexMap[45017] = 17;
	UIDToIndexMap[45018] = 11;
	UIDToIndexMap[45019] = 18;
	UIDToIndexMap[45020] = 19;
	UIDToIndexMap[45021] = 20;
	UIDToIndexMap[45022] = 21;
	UIDToIndexMap[45023] = 22;
	UIDToIndexMap[45024] = 23;
}

void UnlockablesCache::CacheAmmoProviderIndices()
{
	UIDToIndexMap[90000] = 0;
	UIDToIndexMap[90001] = 1;
	UIDToIndexMap[90002] = 2;
	UIDToIndexMap[90003] = 3;
	UIDToIndexMap[90004] = 4;
	UIDToIndexMap[90005] = 6;
	UIDToIndexMap[90006] = 17;
	UIDToIndexMap[90007] = 18;
	UIDToIndexMap[90008] = 5;
	UIDToIndexMap[90009] = 7;
	UIDToIndexMap[90010] = 8;
	UIDToIndexMap[90011] = 9;
	UIDToIndexMap[90012] = 10;
	UIDToIndexMap[90013] = 11;
	UIDToIndexMap[90014] = 12;
	UIDToIndexMap[90015] = 13;
	UIDToIndexMap[90016] = 14;
	UIDToIndexMap[90017] = 15;
	UIDToIndexMap[90018] = 16;
}

void UnlockablesCache::CacheStockProviderIndices()
{
	UIDToIndexMap[42000] = 0;
	UIDToIndexMap[42001] = 1;
	UIDToIndexMap[42002] = 2;
	UIDToIndexMap[42003] = 3;
	UIDToIndexMap[42004] = 4;
	UIDToIndexMap[42005] = 5;
	UIDToIndexMap[42006] = 6;
	UIDToIndexMap[42007] = 7;
	UIDToIndexMap[42008] = 8;
	UIDToIndexMap[42009] = 9;
	UIDToIndexMap[42010] = 10;
	UIDToIndexMap[42011] = 11;
	UIDToIndexMap[42012] = 12;
	UIDToIndexMap[42013] = 13;
	UIDToIndexMap[42014] = 14;
	UIDToIndexMap[42015] = 15;
	UIDToIndexMap[42016] = 16;
	UIDToIndexMap[42017] = 17;
	UIDToIndexMap[42018] = 18;
	UIDToIndexMap[42019] = 19;
	UIDToIndexMap[42020] = 20;
	UIDToIndexMap[42021] = 21;
}

void UnlockablesCache::CacheGripProviderIndices()
{
	UIDToIndexMap[62000] = 0;
	UIDToIndexMap[62001] = 1;
}

void UnlockablesCache::CacheWeaponModProviderIndices(TArray<FString> modNames, int modUidBase)
{
	int i = 0;
	for (auto modName : modNames) {
		UIDToIndexMap[modUidBase + i] = i;
		i += 1;
	}
}

void UnlockablesCache::CacheDataProviderIndices(TArray<UFoxDataProvider_Unlockable*>* provider)
{
	for (int i = 0; i < provider->Count; i++)
	{
		auto unlockable = provider->at(i);
		UIDToIndexMap[unlockable->UnlockID] = i;
		UnlockInfos[unlockable->UnlockID] = unlockable;
	}
}

void UnlockablesCache::ConvertWeaponData(FLoadoutInfoData& loadoutData)
{
#define UIDTOID(name) loadoutData.WeaponData.##name##ID = GetProviderIndexFromUID(loadoutData.WeaponUnlocks.##name##UID)
#define WPNUIDTOID(type) \
	UIDTOID(type##Skin); \
	UIDTOID(type##Weapon); \
	UIDTOID(type##Muzzle); \
	UIDTOID(type##Barrel); \
	UIDTOID(type##Magazine); \
	UIDTOID(type##Scope); \
	UIDTOID(type##Stock); \
	UIDTOID(type##Grip); \
	UIDTOID(type##Ammo); \
	loadoutData.WeaponData.##type##WeaponCamo = GetProviderIndexFromUID(loadoutData.WeaponUnlocks.##type##WeaponCamoUID);

	WPNUIDTOID(Primary);
	WPNUIDTOID(Secondary);
	UIDTOID(Hanger);

#undef UIDTOID
#undef WPNUIDTOID
}

void UnlockablesCache::ConvertGearData(FLoadoutInfoData& loadoutData)
{
#define UIDTOID(name) loadoutData.GearData.##name##ID = GetProviderIndexFromUID(loadoutData.GearUnlocks.##name##UID)

	auto unlockInfo = UObject::GetInstanceOf<UFoxUnlockInfo>(true);
	UIDTOID(BodyCamo);
	UIDTOID(UpperBody);
	UIDTOID(LowerBody);
	UIDTOID(Helmet);
	UIDTOID(Badge);
	UIDTOID(Gear_R1);
	UIDTOID(Gear_R2);
	UIDTOID(Gear_L1);
	UIDTOID(Gear_L2);
	UIDTOID(Tactical);
	UIDTOID(ButtPack);
	UIDTOID(Avatar);
	UIDTOID(Hanger);

	loadoutData.GearData.PatchIconID = loadoutData.GearUnlocks.PatchIconUID;
	loadoutData.GearData.PatchIconColorID = unlockInfo->GetEmblemComponentIndexFromUnlockID(loadoutData.GearUnlocks.PatchIconColorUID);
	loadoutData.GearData.PatchShapeID = unlockInfo->GetEmblemShapeID(loadoutData.GearUnlocks.PatchShapeUID);
	loadoutData.GearData.PatchShapeColorID = unlockInfo->GetEmblemComponentIndexFromUnlockID(loadoutData.GearUnlocks.PatchShapeColorUID);
	loadoutData.GearData.bFemale = loadoutData.GearUnlocks.bFemale;
	loadoutData.GearData.bBot = false;
#undef UIDTOID
}

void UnlockablesCache::ConvertDepotData(FLoadoutInfoData& loadoutData)
{
	loadoutData.DepotData.DepotPresetID0 = GetProviderIndexFromUID(loadoutData.DepotUnlocks.DepotUIDs[0], 0);
	loadoutData.DepotData.DepotPresetID1 = GetProviderIndexFromUID(loadoutData.DepotUnlocks.DepotUIDs[1], 0);
	loadoutData.DepotData.DepotPresetID2 = GetProviderIndexFromUID(loadoutData.DepotUnlocks.DepotUIDs[2], 0);
	loadoutData.DepotData.DepotPresetID3 = GetProviderIndexFromUID(loadoutData.DepotUnlocks.DepotUIDs[3], 0);
	loadoutData.DepotData.DepotPresetID4 = GetProviderIndexFromUID(loadoutData.DepotUnlocks.DepotUIDs[4], 0);
}